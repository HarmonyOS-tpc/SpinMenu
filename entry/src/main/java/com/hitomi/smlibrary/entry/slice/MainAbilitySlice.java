package com.hitomi.smlibrary.entry.slice;

import com.hitomi.smlibrary.OnSpinMenuStateChangeListener;
import com.hitomi.smlibrary.SpinMenu;
import com.hitomi.smlibrary.entry.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    private SpinMenu spinMenu;
    private int[] imgIds = new int[]{
            ResourceTable.Media_bg_fragment_1,
            ResourceTable.Media_bg_fragment_2,
            ResourceTable.Media_bg_fragment_3,
            ResourceTable.Media_bg_fragment_4,
            ResourceTable.Media_bg_fragment_5,
            ResourceTable.Media_bg_fragment_6,
            ResourceTable.Media_bg_fragment_7,
            ResourceTable.Media_bg_fragment_8
    };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        spinMenu = (SpinMenu) findComponentById(ResourceTable.Id_spin_menu);

        List<String> hintStrList = new ArrayList<>();
        hintStrList.add("热门信息");
        hintStrList.add("实时新闻");
        hintStrList.add("我的论坛");
        hintStrList.add("我的信息");
        hintStrList.add("走走看看");
        hintStrList.add("阅读空间");
        hintStrList.add("听听唱唱");
        hintStrList.add("系统设置");

        spinMenu.setHintTextStrList(hintStrList);
        spinMenu.setHintTextColor(0xffffffff);
        spinMenu.setHintTextSize(dp2px(14));

        // 设置启动手势开启菜单
        spinMenu.setEnableGesture(true);

        // 设置页面适配器
        final List<Component> components = new ArrayList<>();
        for (int i = 0; i < imgIds.length; i++) {
            Image image = new Image(this);
            ComponentContainer.LayoutConfig layoutConfig = image.getLayoutConfig();
            layoutConfig.height = ComponentContainer.LayoutConfig.MATCH_PARENT;
            layoutConfig.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
            image.setLayoutConfig(layoutConfig);
            image.setPixelMap(imgIds[i]);
            image.setScaleMode(Image.ScaleMode.STRETCH);
            components.add(image);
        }

        PageSliderProvider pageSliderProvider = new PageSliderProvider() {
            @Override
            public int getCount() {
                return components.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                return components.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {

            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return false;
            }
        };
        spinMenu.setFragmentAdapter(pageSliderProvider);

        // 设置菜单状态改变时的监听器
        spinMenu.setOnSpinMenuStateChangeListener(new OnSpinMenuStateChangeListener() {
            @Override
            public void onMenuOpened() {
            }

            @Override
            public void onMenuClosed() {
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    // 测试Click事件冲突
    private void test(List<Component> components) {
        DirectionalLayout directionalLayout = new DirectionalLayout(this);
        ComponentContainer.LayoutConfig layoutConfig2 = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        directionalLayout.setLayoutConfig(layoutConfig2);
        Text text = new Text(this);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setText("哈哈哈");
        text.setTextSize(40);
        text.setTextColor(new Color(0xffff5454));
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(0xff00ff00));
        ComponentContainer.LayoutConfig layoutConfig = text.getLayoutConfig();
        layoutConfig.height = ComponentContainer.LayoutConfig.MATCH_PARENT;
        layoutConfig.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
        text.setBackground(shapeElement);
        text.setLayoutConfig(layoutConfig);
        directionalLayout.addComponent(text);
        components.add(directionalLayout);

        text.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                text.setText(text.getText() + "哈哈哈");
            }
        });
    }

    private int dp2px(float dp) {
        return (int) (getResourceManager().getDeviceCapability().screenDensity / 160 * dp);
    }
}
