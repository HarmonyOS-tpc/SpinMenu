# SpinMenu

基于开源组件项目：https://github.com/Hitomis/SpinMenu，开发的openharmony开源组件


轮盘样式的 Component 菜单选择控件。有没有很炫？

# Preview

<img src="preview/demo.gif"/>


# Usage

```groovy
dependencies{
       implementation 'io.openharmony.tpc.thirdlib:SpinMenu:1.0.1'
}
```

导入 smlibrary module, 或者直接拷贝 com.hitomi.smlibrary 包下所有 java 文件到您的项目中

布局文件中：
    <com.hitomi.smlibrary.SpinMenu
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        xmlns:app="style"
        ohos:id="$+id:spin_menu"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:background_element="#333a4a"
        app:hint_text_color="#FFFFFF"
        app:hint_text_size="14sp"
        app:scale_ratio="0.36">
    </com.hitomi.smlibrary.SpinMenu>

如果您觉得背景太空洞，可以在 SpinMenu 中嵌套其它布局，来绘制您自己的背景

Ability 或 AbilitySlice 中：

    spinMenu = (SpinMenu) findComponentById(ResourceTable.Id_spin_menu);

    // 设置页面标题
    List<String> hintStrList = new ArrayList<>();
    hintStrList.add("热门信息");
    hintStrList.add("实时新闻");
    hintStrList.add("我的论坛");
    hintStrList.add("我的信息");
    hintStrList.add("走走看看");
    hintStrList.add("阅读空间");
    hintStrList.add("听听唱唱");
    hintStrList.add("系统设置");

    spinMenu.setHintTextStrList(hintStrList);
    spinMenu.setHintTextColor(0xffffffff);
    spinMenu.setHintTextSize(dp2px(14));

    // 设置启动手势开启菜单
    spinMenu.setEnableGesture(true);

    // 设置页面适配器，由于openharmony没有合适适配的Fragment，这里直接适配了Component，Layout也可以直接使用可操作性更大
    final List<Component> components = new ArrayList<>();
        for (int i = 0; i < imgIds.length ; i++) {
            Image image = new Image(this);
            ComponentContainer.LayoutConfig layoutConfig = image.getLayoutConfig();
            layoutConfig.height = ComponentContainer.LayoutConfig.MATCH_PARENT;
            layoutConfig.width = ComponentContainer.LayoutConfig.MATCH_PARENT;
            image.setLayoutConfig(layoutConfig);
            image.setPixelMap(imgIds[i]);
            image.setScaleMode(Image.ScaleMode.STRETCH);
            components.add(image);
        }

        PageSliderProvider pageSliderProvider = new PageSliderProvider() {
            @Override
            public int getCount() {
                return components.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                return components.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {

            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return false;
            }
        };
        spinMenu.setFragmentAdapter(pageSliderProvider);

    // 设置菜单状态改变时的监听器
        spinMenu.setOnSpinMenuStateChangeListener(new OnSpinMenuStateChangeListener() {
            @Override
            public void onMenuOpened() {
                // menu open
            }

            @Override
            public void onMenuClosed() {
                // menu open
            }
        });

# AttrSet

|name|format|description|
|:---:|:---:|:---:|
| scale_ratio | float | 支持页面缩放大小自定义，建议取值在0.3到0.5之间
| hint_text_color | color | 支持页面标题文字颜色自定义
| hint_text_size | dimension | 支持页面标题文字大小自定义

#License

MIT
 


