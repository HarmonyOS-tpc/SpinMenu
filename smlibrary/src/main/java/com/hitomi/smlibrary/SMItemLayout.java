package com.hitomi.smlibrary;

import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

/**
 * Created by hitomi on 2016/9/18.
 */
public class SMItemLayout extends DirectionalLayout {

    public SMItemLayout(Context context) {
        this(context, null);
    }

    public SMItemLayout(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public SMItemLayout(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setOrientation(VERTICAL);
    }
}
